/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab05;

import java.util.Scanner;

/**
 *
 * @author User
 */
public class Game {

    private Player player1;
    private Player player2;
    private Player currentPlayer ;
    private Table table;
    private boolean isFinish = false;
    private String playagain;

    public Game() {
        this.player1 = new Player("X");
        this.player2 = new Player("O");
    }

    public void playGame() {
        printWelCome();
        newGame();
        printTable();
        while (!isFinish) {
            printTurn();
            inputPosition();
            printTable();
            if (table.checkWin()) {
                printTable();
                printWinner();
                printPlayAgain();
            } else if (table.checkDraw()) {
                printTable();
                printDraw();
                printPlayAgain();
            }
            table.switchPlayer();
        }
    }

    private void printWelCome() {
        System.out.println("Welcome to XO Game !!!");
    }

    private void printTable() {
        String[] tb = table.getTable();
        System.out.println();
        System.out.println(tb[0] + " " + tb[1] + " " + tb[2] + " ");
        System.out.println(tb[3] + " " + tb[4] + " " + tb[5] + " ");
        System.out.println(tb[6] + " " + tb[7] + " " + tb[8] + " ");
    }

    private void printTurn() {
        System.out.println(table.getCurrentPlayer().getSymbol() + " Turn ");
    }

    private void inputPosition() {
        Scanner kb = new Scanner(System.in);
        boolean stop = true;
        int position;
        while (stop) {
            System.out.print("Please input number :");
            position = kb.nextInt();
            if (table.setPosition(position)) {
                stop = false;
            }
        }
        stop = true;

    }

    private void printWinner() {
        System.out.println("Congratulations " + table.getCurrentPlayer().getSymbol() + " is the winner !!!"); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    private void printDraw() {
        System.out.println("Draw!!!"); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    private void newGame() {
        table = new Table(player1, player2); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    private void printPlayAgain() {
        System.out.println("Continue Y/N ?");
        Scanner kb = new Scanner(System.in); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
        playagain = kb.next();
        if (playagain.equals("Y") || playagain.equals("y")) {
            table.reSetXO();
            setDefaultPlayer();
            printTable();
        } else if (playagain.equals("N") || playagain.equals("n")) {
            isFinish = true;
        }
    }

    private void setDefaultPlayer() {
        table.resetCurrentPlayer();
        playagain = "";

    }

}
